#pragma once

#include <hyprland/src/plugins/PluginAPI.hpp>

#include "SlidrLayout.hpp"

inline HANDLE PHANDLE = nullptr;
inline std::unique_ptr<SlidrLayout> g_SlidrLayout;

#include <hyprland/src/debug/Log.hpp>
#include <hyprland/src/plugins/PluginAPI.hpp>

#include "globals.hpp"
#include "dispatchers.hpp"
#include "log.hpp"
#include "SlidrLayout.hpp"

APICALL EXPORT std::string PLUGIN_API_VERSION() {
    return HYPRLAND_API_VERSION;
}

APICALL EXPORT PLUGIN_DESCRIPTION_INFO PLUGIN_INIT(HANDLE handle) {
    PHANDLE = handle;

    Debug::disableStdout = false;
    slidr_log(INFO, "loading slidr");

    g_SlidrLayout = std::make_unique<SlidrLayout>();
    HyprlandAPI::addLayout(PHANDLE, "slidr", g_SlidrLayout.get());

    dispatchers::addDispatchers();

    return {"hyprslidr", "sliding window layout", "magthe", "0.1"};
}

APICALL EXPORT void PLUGIN_EXIT() {}

#pragma once

#include <hyprland/src/layout/IHyprLayout.hpp>

#include "Slider.hpp"

class SlidrLayout : public IHyprLayout {
  public:
    virtual void onWindowCreatedTiling(CWindow*, eDirection = DIRECTION_DEFAULT);
    virtual void onWindowRemovedTiling(CWindow*);
    virtual void onWindowFocusChange(CWindow*);
    virtual bool isWindowTiled(CWindow*);
    virtual void recalculateMonitor(const int& monitor_id);
    virtual void recalculateWindow(CWindow*);
    virtual void resizeActiveWindow(const Vector2D& delta, eRectCorner corner, CWindow* pWindow = nullptr);
    virtual void fullscreenRequestForWindow(CWindow*, eFullscreenMode, bool enable_fullscreen);
    virtual std::any layoutMessage(SLayoutMessageHeader header, std::string content);
    virtual SWindowRenderLayoutHints requestRenderHints(CWindow*);
    virtual void switchWindows(CWindow*, CWindow*);
    virtual void moveWindowTo(CWindow*, const std::string& direction);
    virtual void alterSplitRatio(CWindow*, float, bool);
    virtual std::string getLayoutName();
    virtual CWindow* getNextWindowCandidate(CWindow*);
    virtual void replaceWindowDataWith(CWindow* from, CWindow* to);

    virtual void onEnable();
    virtual void onDisable();

    void cycleWindowSize(int workspace);
    void moveFocus(int workspace, Direction);
    void alignWindow(int workspace, Direction);
    void admitWindowLeft(int workspace);
    void expelWindowRight(int workspace);
    void printLayout();

  private:
    Slider* getSliderForWorkspace(int workspace);
    Slider* getSliderForWindow(CWindow* window);

    std::list<std::unique_ptr<Slider>> sliders;
};

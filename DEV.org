** Running nested Hyprland

#+begin_src shell
Hyprland -c =(cat hypr.conf.in | envsubst '$PWD')
#+end_src

** Multiple monitors in nested Hyprland

Put multiple monitors in the config

#+begin_src conf
monitor = WL-1, 960x540, auto, 1
monitor = WL-2, 960x540, auto, 1
#+end_src

then use the (undocumented) command =hyprctl output= to control the outputs.

To create use

#+begin_src shell
hyprctl --instance 1 output create auto
#+end_src

To destroy use

#+begin_src shell
hyprctl --instance 1 output destroy WL-2
#+end_src
